//
//  TableDataSource.swift
//  iOS Test Tunaiku
//
//  Created by user on 10/01/19.
//  Copyright © 2019 Tunaiku. All rights reserved.
//

import UIKit

class TableDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var viewModel = [ViewModel]()
    
    func registerTableview(_ caller: UIViewController, tableview: UITableView) {
        tableview.register(UINib(nibName: "TableCell", bundle: caller.nibBundle), forCellReuseIdentifier: "TableCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as! TableViewCell
        cell.setupNotes(title: self.viewModel[indexPath.row].title ?? "" , body: self.viewModel[indexPath.row].detail ?? "")
        return cell
    }
}
