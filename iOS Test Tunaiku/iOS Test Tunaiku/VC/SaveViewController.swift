//
//  SaveViewController.swift
//  iOS Test Tunaiku
//
//  Created by user on 15/01/19.
//  Copyright © 2019 Tunaiku. All rights reserved.
//

import UIKit

protocol SaveProtocol {
    func saveNote(_ title: String, detail: String)
}

class SaveViewController: UIViewController {

    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var detailField: UITextField!
    var saveDelegate: SaveProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveAction(_ sender: Any) {
        self.saveDelegate?.saveNote(self.titleField.text!, detail: self.detailField.text!)
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
