//
//  ViewController.swift
//  iOS Test Tunaiku
//
//  Created by user on 10/01/19.
//  Copyright © 2019 Tunaiku. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var dataSource: TableDataSource?
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.dataSource = dataSource
        tableview.delegate = dataSource
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func addAction(_ sender: Any) {
        performSegue(withIdentifier: "add_segue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "add_segue" {
            let sb = UIStoryboard(name: "Main", bundle: self.nibBundle)
            let vc = sb.instantiateViewController(withIdentifier: "SaveViewController") as! SaveViewController
            vc.saveDelegate = self
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
}

extension ViewController: SaveProtocol{
    func saveNote(_ title: String, detail: String) {
        print(title)
        
        self.dataSource?.viewModel.append(ViewModel(title: title, detail: detail))

        self.tableview.reloadData()
    }
    
    
}
