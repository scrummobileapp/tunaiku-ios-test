//
//  TableViewCell.swift
//  iOS Test Tunaiku
//
//  Created by user on 10/01/19.
//  Copyright © 2019 Tunaiku. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var titleNotes: UILabel!
    @IBOutlet weak var bodyNotes: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupNotes(title: String, body: String) {
        self.titleNotes.text = title
        self.bodyNotes.text = body
    }

}
