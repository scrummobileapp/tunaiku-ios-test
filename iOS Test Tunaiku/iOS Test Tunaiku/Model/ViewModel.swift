//
//  ViewModel.swift
//  iOS Test Tunaiku
//
//  Created by user on 15/01/19.
//  Copyright © 2019 Tunaiku. All rights reserved.
//

import Foundation

class ViewModel {
    var title: String! = nil
    var detail: String! = nil
    
    init(title: String? = "", detail: String? = ""){
        self.title = title
        self.detail = detail
    }
}
